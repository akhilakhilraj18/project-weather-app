import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import axios from 'axios';

function App(){
  const [city,setcity] = useState('New York')
   const [temperature,setTemperature] = useState(0)
   const [loading,setloading] = useState(false)

  function selectcity(cityname, latitude,longitude){
    setcity(cityname)
    setloading(true)

    axios.get('https://api.open-meteo.com/v1/forecast?latitude='+latitude+'&longitude='+longitude+'&current_weather=true&hourly=temperature_2m,relativehumidity_2m,windspeed_10m')
    .then(data=>{
      const temperaturevalue = data.data.current_weather.temperature
      setTemperature(temperaturevalue)
      setloading(false)
    })
    .catch(err=>console.log(err))
    
  }
  

    return (
    <div className="App">
      <h1>My weather app</h1>
      <div>
        <button onClick={()=>{selectcity('Kollam',8.89,76.61)}}>Kollam</button>
        <button onClick={()=>{selectcity('Trivandrum',8.52,76.93)}}>Trivandrum</button>
        <button onClick={()=>{selectcity('Calicut',11.25, 75.78)}}>Calicut</button>
      </div>
    
    {loading?<p>Loading...</p>:<p>The current temperature at <span id='City'>{city}</span> is &nbsp;&nbsp;<span id='temperature'>{temperature}°C</span></p>}
    </div>
  );
    }


export default App;
